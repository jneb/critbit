# critbit #
Ths is the Python version of the critbit tree as described
at https://cr.yp.to/critbit.html


### What is this repository for? ###
This code, although perfectly running and self testing,
is not useful in Python.
From a Python standpoint, it is just a
very slow and lame dict, that only accepts
nonnegative integers as keys.
The goal is to describe the ideas behind the critbit;
especially the insert, which is a bit tricky.

### How do I get set up? ###
It is just a single file without dependencies.
You can import it as a module, or run to test itself.

### Contribution guidelines ###

Drop me a line!

### Who do I talk to? ###

Jurjen N.E. Bos
