#!python3
"""A demo implementation of crit-bit trees
First for numbers, then for strings.
"""
import unittest, random

class NumCritBit:
    """A NumCritBit tree consists of nodes of three elements.
    Meaning depends on .left:
        leaf node:
            left: None
            middle: The value
            right: The associated value, if any
        inner node:
            left: subtree: elements with (value >> self.critbit) & 1 == 0
            middle: critbit
            right: subtree: elements with (value >> self.critbit) & 1 == 1
    This implies:
        all items in the tree have the same value if shifted right by critbit + 1
        all left items < all right items

    Note: Tree updates are done in place to save memory shuffling
    All operations (except __iter__ and such) are logaritmic,
    linear operations are half-recursive.
    """
    __slots__ = 'left', 'middle', 'right'
    def __init__(self, initialItem, value=None):
        """Make a new tree with given item, with optional association"""
        if initialItem < 0:
            raise ValueError("Negative items not allowed")
        self.left = None
        self.middle = initialItem
        self.right = value

    @property
    def contents(self):
        if self.left is not None:
            raise ValueError("Not a leaf node")
        return self.middle

    @property
    def critbit(self):
        if self.left is None:
            raise ValueError("Not an inner node")
        return self.middle

    def __str__(self):
        return 'NumCritBit(' + (
            ','.join(map(str, self))
            ) + ')'

    def __repr__(self):
        """Represent, showing one or two items
        """
        if (self.left is None):
            return f"NumCritBit({self.middle})"
        elif self.left.left is None and self.right.left is None:
            return f"NumCritBit({self.min()},{self.max()})"
        else:
            return f"NumCritBit({self.min()}..{self.max()}: {len(self)} items)"

    def __len__(tree):
        """Count number of elements
        Algorithm is recursive to the right, but loops to the left.
        """
        # start with one element for the leaf node
        total = 1
        while tree.left is not None:
            total += len(tree.right)
            tree = tree.left
        # we are now at a leaf node
        return total

    def copy(self, deep=True):
        """Make a copy of a tree.
        Using deep=False is only for internal use!
        """
        result = NumCritBit(self.middle)
        if self.left is None:
            result.right = self.right
        elif deep:
            result.left = self.left.copy()
            result.right = self.right.copy()
        else:
            result.left = self.left
            result.right = self.right
        return result

    def min(tree):
        """Minimum entry
        First item of __iter__
        """
        while tree.left is not None: tree = tree.left
        return tree.middle

    def max(tree):
        """Maximum entry
        Last item of __iter__
        """
        while tree.left is not None: tree = tree.right
        return tree.middle

    def __contains__(self, item):
        """Check if item is in tree"""
        return self.matchingLeaf(item).middle == item

    def matchingLeaf(tree, item):
        """Find the leaf node you get if you just check the critbits
        May or may not contain the given item
        """
        while tree.left is not None:
            # split at critbit
            tree = tree.right if (item >> tree.middle) & 1 else tree.left
        return tree

    def __delitem__(tree, item):
        """Delete given item"""
        while tree.left is not None:
            # split at critbit to see which subtree to search
            if (item >> tree.middle) & 1:
                thisTree, otherTree = tree.right, tree.left
            else:
                thisTree, otherTree = tree.left, tree.right
            # now see what we have to do
            if thisTree.left is not None:
                # entry to remove is in thisTree: recurse
                tree = thisTree
            elif thisTree.middle != item:
                # entry wasn't in the tree
                raise ValueError("Item {} not in tree".format(item))
            else:
                # found leaf item in thisTree, so replace tree by otherTree
                tree.left, tree.middle, tree.right = (
                    otherTree.left, otherTree.middle, otherTree.right)
                return
        # tree was empty
        raise ValueError("Can't remove last item")

    def __setitem__(tree, item, value=None):
        """Change associated value for an item in the tree
        """
        if item < 0:
            raise ValueError("Negative items not allowed")
        # check if item in the tree;
        # also: get an item in the tree matching all relevant critbits
        matchTree = tree.matchingLeaf(item)
        if item == matchTree.middle:
            # we have the right location in the tree, just set value
            matchTree.right = value
            return
        # new item must be placed somewhere in the path to matching value
        # find the new critbit
        critbit = (item ^ matchTree.middle).bit_length() - 1
        # follow path to matching value
        # then compute the new left and right branches for tree
        while tree.left is not None:
            if critbit > tree.middle: break
            # go deeper
            tree = tree.right if (item >> tree.middle) & 1 else tree.left
        # tree is now the node where the item should be inserted
        # get new subtrees, assuming the new node is at the left
        left, right = NumCritBit(item, value), tree.copy(deep=False)
        # if item's critbit is 1, swap the subtrees
        if (item >> critbit) & 1:
            left, right = right, left
        # set up tree as internal node with given left and right subtrees
        tree.left, tree.middle, tree.right = left, critbit, right

    def __getitem__(tree, item, value=None):
        """Change associated value for an item in the tree
        """
        while tree.left is not None:
            # split at critbit
            tree = tree.right if (item >> tree.middle) & 1 else tree.left
        if tree.middle == item:
            return tree.right
        raise KeyError(f"Item {item} not found")

    def add(self, item):
        """Add element to the tree, associating to None
        """
        self[item] = None

    def minAbove(tree, item):
        """Smallest item above given
        Quadratic in log(n)
        """
        # get an item in the tree matching all relevant critbits with item - 1
        matchItem = tree.matchingLeaf(item).middle
        # find the critbit where the item differs from a subtree
        critbit = (item ^ matchItem).bit_length() - 1
        # follow the path to the place where the entry should be
        # remembering last right branch
        lastRight = None
        while tree.left is not None:
            if critbit > tree.middle: break
            # go deeper, remember last left subtree
            if (item >> tree.middle) & 1:
                tree = tree.right
            else:
                lastRight = tree.right
                tree = tree.left
        if matchItem <= item:
            # we are exactly at item, or subtree values are all lower
            return lastRight.min()
        else:
            # the subtree consists of lower values
            return tree.min()

    def maxBelow(tree, item):
        """Highest item below given
        """
        # get an item in the tree matching all relevant critbits with item - 1
        matchItem = tree.matchingLeaf(item).middle
        # find the critbit where the item differs from a subtree
        critbit = (item ^ matchItem).bit_length() - 1
        # follow the path to the place where the entry should be
        # remembering last left branch
        lastLeft = None
        while tree.left is not None:
            if critbit > tree.middle: break
            # go deeper, remember last left subtree
            if (item >> tree.middle) & 1:
                lastLeft = tree.left
                tree = tree.right
            else:
                tree = tree.left
        if matchItem >= item:
            # we are exactly at item, or subtree values are all higher
            return lastLeft.max()
        else:
            # the subtree consists of lower values
            return tree.max()

    def suffix(tree, value, shift):
        """Find subtree with all items which are equal to value if shifted to the right
        or None if there are no such items
        """
        # search subtree
        while tree.left is not None:
            if tree.middle < shift:
                break
            tree = tree.right if (value >> tree.middle - shift) & 1 else tree.left
        else:
            # found one entry, output if relevant
            return tree if value == tree.middle >> shift else None
        # found tree, output if relevant. Use matchItem as a representative
        matchItem = tree.matchingLeaf(value << shift).middle
        return tree if value == matchItem >> shift else None

    def __iter__(tree):
        """Iterate in order of reversed bit strings
        """
        while tree.left is not None:
            yield from tree.left
            tree = tree.right
        yield tree.middle

    def __reversed__(tree):
        """Reversed iterate in order of reversed bit strings
        """
        while tree.left is not None:
            yield from reversed(tree.right)
            tree = tree.left
        yield tree.middle

    def items(tree):
        """Like dict.items
        """
        while tree.left is not None:
            yield from tree.left.items()
            tree = tree.right
        yield tree.middle, tree.right


class TestNumCritbit(unittest.TestCase):
    def makeRandom(self, size=25, maxValue=100):
        numbers = iter(random.sample(range(maxValue), size))
        result = NumCritBit(next(numbers))
        for i,n in enumerate(numbers):
            result[n] = i
        return result

    testValue = makeRandom(None)

    def checkNumCritbit(self, tree, maxCritbit=None):
        """Check if the data structure still works,
        and if the critbits are below a given value.
        For testing.
        """
        if tree.left is not None:
            if maxCritbit is not None:
                self.assertLess(tree.middle, maxCritbit)
            self.checkNumCritbit(tree.left, tree.middle)
            self.checkNumCritbit(tree.right, tree.middle)
            maxLeft, minRight = max(tree.left), min(tree.right)
            self.assertLess(maxLeft, minRight)
            self.assertEqual((maxLeft >> tree.middle) & 1, 0)
            self.assertEqual((minRight >> tree.middle) & 1, 1)

    def testLeaf(self):
        "Check leaf nodes"
        t = NumCritBit(6, 10)
        self.assertIs(t.left, None)
        self.assertEqual(t.middle, 6)
        self.assertEqual(t.right, 10)
        t = NumCritBit(9)
        self.assertIs(t.left, None)
        self.assertEqual(t.middle, 9)
        self.assertIs(t.right, None)

    def testTree(self):
        "check internal nodes"
        self.checkNumCritbit(self.testValue)

    def testProperties(self):
        "check contents and critbit properties"
        leaf, tree = self.makeRandom(1), self.makeRandom(3)
        self.assertEqual(leaf.middle, leaf.contents)
        with self.assertRaises(ValueError):
            leaf.critbit
        self.assertEqual(tree.middle, tree.critbit)
        with self.assertRaises(ValueError):
            tree.contents

    def testStr(self):
        "test str and repr"
        t = NumCritBit(10)
        self.assertEqual(str(t), "NumCritBit(10)")
        self.assertEqual(repr(t), "NumCritBit(10)")
        t.add(15)
        self.assertEqual(str(t), "NumCritBit(10,15)")
        self.assertEqual(repr(t), "NumCritBit(10,15)")
        t.add(3)
        self.assertEqual(str(t), "NumCritBit(3,10,15)")
        self.assertEqual(repr(t), "NumCritBit(3..15: 3 items)")
        t.add(12)
        self.assertEqual(str(t), "NumCritBit(3,10,12,15)")
        self.assertEqual(repr(t), "NumCritBit(3..15: 4 items)")

    def testLen(self):
        "simple check of len"
        self.assertEqual(len(self.testValue), 25)

    def testCopy(self):
        "check deep copy"
        t = NumCritBit(10)
        for i in range(1, 10): t.add(i)
        u = t.copy()
        t.add(11)
        self.assertEqual(len(t), 11)
        self.assertEqual(len(u), 10)

    def testAddDel(self):
        "test add and del"
        t = NumCritBit(10)
        s = {10: None}
        for i in range(50):
            r = random.randrange(100)
            t[r] = i
            s[r] = i
            self.assertEqual(set(t.items()), set(s.items()))
        for i in range(1, len(t)):
            r = random.choice(list(s))
            del t[r]
            del s[r]
            self.assertEqual(set(t.items()), set(s.items()))
        with self.assertRaises(ValueError):
            del t[t.min()]

    def testSort(self):
        "test sorted order and reversed"
        l = sorted(self.testValue)
        self.assertEqual(list(self.testValue), l)
        self.assertEqual(list(reversed(self.testValue)), l[::-1])

    def testSplit(self):
        "test suffix"
        l = list(self.testValue)
        for i in range(13):
            group = self.testValue.suffix(i, 3)
            correct = [x for x in l if 8*i <= x < 8*(i+1)]
            if group is None:
                self.assertFalse(correct)
            else:
                self.assertEqual(list(group), correct)

    def testContains(self):
        "test contains"
        l = set(self.testValue)
        for i in range(100):
            self.assertEqual(i in l, i in self.testValue)

    def testAbove(self):
        "Test minAbove, maxBelow"
        t = self.testValue
        it = iter(t)
        # compute first values by hand
        checkLo, checkHi = next(it), next(it)
        # exception: if checkHi == t.min+1, take the next
        for i in range(t.min() + 1, t.max()):
            # step checkHi if needed
            if checkHi == i: checkHi = next(it)
            lo, hi = t.maxBelow(i), t.minAbove(i)
            self.assertEqual(lo, checkLo, (lo, i, hi, list(t)))
            self.assertEqual(hi, checkHi, (lo, i, hi, list(t)))
            # increment checkLo when needed
            if i in t:
                checkLo = i

    def testNeg(self):
        t = NumCritBit(2)
        with self.assertRaises(ValueError):
            t.add(-1)
        with self.assertRaises(ValueError):
            t = NumCritBit(-1)

if __name__ == '__main__':
    import doctest
    doctest.testmod()
    unittest.main()